import os
import json


# ------------------------------------------------------------------------------------------------------------------- #

def readJSON(filepath=None):
    """
    Reads a json file and return the parsed python dictionary

    :param filepath:
    :return:
    """

    assert filepath, "No filepath defined"
    assert isinstance(filepath, basestring), "Wrong string type used for the filepath"


    if not os.path.exists(filepath):
        raise IOError("The filepath [{filepath}] specified doesn't exists.".format(filepath=filepath))

    with open(filepath) as content:
        data = json.load(content)


    return data or False

# ------------------------------------------------------------------------------------------------------------------- #

def writeJSON(filepath=None, contents=None):
    """
    Writes the json contents into the specified filepath

    :param filepath:
    :param contents:
    :return:
    """

    assert filepath, "No filepath defined"
    assert isinstance(filepath, basestring), "Wrong string type used for the filepath"

    assert contents, "No contents for the JSON file defined"
    assert isinstance(contents, dict), "The json contents has to be a dictionary"


    filedir = os.path.dirname(filepath)
    if not os.path.exists(filedir):
        os.makedirs(filedir)

    with open(filepath, 'w') as filehandler:
        json.dump(contents, filehandler, sort_keys=True, indent=4, separators=[', ', ': '])


    return filepath

# ------------------------------------------------------------------------------------------------------------------- #