import os



import maya.cmds as cmd


# ------------------------------------------------------------------------------------------------------------------- #

def fileImport(filepath=None):
    """
    Import the speficied file into maya

    :type filepath: basestring
    :param filepath:
    :return:
    """

    assert filepath, "No filepath defined"
    assert isinstance(filepath, basestring), "Wrong string type used for the filepath"


    if not os.path.exists(filepath):
        raise IOError("The filepath {filepath} doesn't exists".format(filepath=filepath))


    namespace = os.path.splitext(os.path.basename(filepath))[0]

    imported_nodes = cmd.file(
        filepath,
        i=True,
        mergeNamespacesOnClash=True,
        namespace=namespace,
        returnNewNodes=True
    )


    return imported_nodes

# ------------------------------------------------------------------------------------------------------------------- #

def fileExport(filepath=None, selection=None):
    """
    Export all the file or just the specified selection into the filepath defined

    :param filepath:
    :param selection:
    :return:
    """

    assert filepath, "No filepath defined"
    assert isinstance(filepath, basestring), "Wrong string type used for the filepath"

    assert isinstance(selection, bool), "You must specify with True or False if the selection needs to be exported"


    # TODO: maybe not the smartest way to change an extension - IMPROVE with something better
    filepath_dir = os.path.dirname(filepath)

    filepath = os.path.splitext(os.path.basename(filepath))[0]
    filepath = '{filepath}.ma'.format(filepath=filepath)

    filepath = os.path.join(filepath_dir, filepath)


    cmd.file(rename=filepath)
    cmd.file(
        force=True,
        exportSelected=True,
        type='mayaAscii'
    )


    return filepath

# ------------------------------------------------------------------------------------------------------------------- #

def fileReference(filepath=None):
    """
    Import the specified file as reference into Maya

    :param filepath:
    :return:
    """
    assert filepath, "No filepath defined"
    assert isinstance(filepath, basestring), "Wrong string type used for the filepath"


    if not os.path.exists(filepath):
        raise IOError("The filepath {filepath} doesn't exists".format(filepath=filepath))


    namespace = os.path.splitext(os.path.basename(filepath))[0]

    imported_nodes = cmd.file(
        filepath,
        reference=True,
        mergeNamespacesOnClash=True,
        namespace=namespace,
        returnNewNodes=True
    )


    return imported_nodes

# ------------------------------------------------------------------------------------------------------------------- #

def fileCopy(source=None, destination=None):
    """
    Copy the source into destination

    :param source:
    :param destination:
    :return:
    """

    assert source, "No source defined"
    assert isinstance(source, basestring), "Wrong string type for the source path"

    assert destination, "No destination defined"
    assert isinstance(destination, basestring), "Wrong string type for the destination path"


    if not os.path.exists(source):
        raise IOError("The source {source} doesn't exists".format(source=source))


    destination_dir = os.path.dirname(destination)

    if not os.path.exists(destination_dir):
        os.makedirs(destination_dir)

    # cmd.copy()

    return True

# ------------------------------------------------------------------------------------------------------------------- #
