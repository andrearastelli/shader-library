"""
Package: shader-library
File: __init__

Author: a.rastelli

"""

import ui.SLUI as slui
reload(slui)



# ------------------------------------------------------------------------------------------------------------------- #

def execute(arg=None):
    """
    Executes the Shader Library tool, docking it into the right side of the interface.
    :type arg: object
    :return:
    """

    window = slui.SLUI(slui.getMayaMainWindow())
    window.run()

# ------------------------------------------------------------------------------------------------------------------- #
