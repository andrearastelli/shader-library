import maya.cmds as cmd


# ------------------------------------------------------------------------------------------------------------------- #

def listShaders():
    """
    Lists all the shaders in Maya and returns a list of them
    :return:
    """
    pass

# ------------------------------------------------------------------------------------------------------------------- #

def listShaderFromSelection():
    """
    If something is selected, and this selection is something that can have a shader assigned
    then the shader will return
    :return:
    """
    pass

# ------------------------------------------------------------------------------------------------------------------- #

def importShaderball():
    """
    Import a shaderball (from the configuration file) into the scene and return the name of the imported node
    :return:
    """
    pass

# ------------------------------------------------------------------------------------------------------------------- #

def renderPreview():
    """
    Setup all the rendering configuration information to generate a preview of the shader applied to a shaderball
    and return the imagepath of the rendered image
    :return:
    """
    pass


