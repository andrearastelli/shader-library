"""
SLImagePreview.py

Dockable panel that shows the image preview of the selected shader.
Can be undocked and resized to increase the image preview size.

It's content changes as the selection on a specific shader changes.
"""

try:
    from PySide2 import QtWidgets as widget
    from PySide2 import QtGui as gui
    from PySide2 import QtCore as core
except ImportError as ie:
    from PySide import QtGui as widget
    from PySide import QtCore as core



# ------------------------------------------------------------------------------------------------------------------- #

class SLImagePreview(widget.QWidget):

    mainlayout = None

    imagepreview = None

    def __init__(self):
        """
        Creates the interface for the image preview
        """
        super(SLImagePreview, self).__init__()

        self.setWindowTitle('Shader Preview')


        self.mainlayout = widget.QVBoxLayout()


        self.imagepreview = widget.QLabel()
        self.imagepreview.setBackgroundRole(gui.QPalette.Base)
        self.imagepreview.setSizePolicy(widget.QSizePolicy.Ignored, widget.QSizePolicy.Ignored)
        self.imagepreview.setScaledContents(True)

        self.mainlayout.addWidget(self.imagepreview)


        self.setLayout(self.mainlayout)

    # -------------------------------------------------------------------------------------------------------------- #

    def loadImage(self, filename=None):
        """

        :param filename:
        :return:
        """

        assert filename, 'No filename defined.'
        assert isinstance(filename, basestring), 'The filename defined is not a string, {} instead'.format(type(filename))


        image = gui.QImage()
        image.load(filename)

        if image.isNull():
            widget.QMessageBox.information(self, 'Shader preview', 'Cannot load {}'.format(filename))


        self.imagepreview.setPixmap(gui.QPixmap.fromImage(image))

# ------------------------------------------------------------------------------------------------------------------- #