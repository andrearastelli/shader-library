"""
Package: shader-library
File: SLUI

Author: a.rastelli

"""

try:
    from PySide2 import QtWidgets as gui
    from PySide2 import QtCore as core
    import shiboken2 as shiboken
except ImportError as ie:
    from PySide import QtGui as gui
    from PySide import QtCore as core
    import shiboken as shiboken


import maya.app.general.mayaMixin as mayaMixin
import maya.cmds as cmd
import maya.OpenMayaUI as omui


import SLTreeList
import SLImagePreview
import SLObjectList



# ------------------------------------------------------------------------------------------------------------------- #

def getMayaMainWindow():
    main_window_pointer = omui.MQtUtil.mainWindow()
    return shiboken.wrapInstance(long(main_window_pointer), gui.QWidget)

# ------------------------------------------------------------------------------------------------------------------- #

class SLUI(mayaMixin.MayaQWidgetDockableMixin, gui.QMainWindow):


    def __init__(self, parent=None):
        """

        :param parent:
        """

        super(SLUI, self).__init__(parent)


        self.setWindowTitle('Shader Library')
        self.setAttribute(core.Qt.WA_DeleteOnClose, True)
        self.setWindowFlags(core.Qt.Tool)


        imagepreview = SLImagePreview.SLImagePreview()
        dockwidget_imagepreview = gui.QDockWidget()
        dockwidget_imagepreview.setWidget(imagepreview)
        self.addDockWidget(core.Qt.LeftDockWidgetArea, dockwidget_imagepreview)


        treelist = SLTreeList.SLTreeList()
        dockwidget_treelist = gui.QDockWidget()
        dockwidget_treelist.setWidget(treelist)
        self.addDockWidget(core.Qt.LeftDockWidgetArea, dockwidget_treelist)


        objectlist = SLObjectList.SLObjectList()
        dockwidget_objectlist = gui.QDockWidget()
        dockwidget_objectlist.setWidget(objectlist)
        self.addDockWidget(core.Qt.RightDockWidgetArea, dockwidget_objectlist)

    # -------------------------------------------------------------------------------------------------------------- #

    def dockCloseEventTriggered(self):
        """
        Defines the necessary operations to close the widget
        :return:
        """
        self.setParent(None)
        self.deleteLater()

    # -------------------------------------------------------------------------------------------------------------- #

    def _deleteControl(self, control_name):
        """
        Deletes the workspace control, if exists.
        :param control_name:
        :return:
        """
        if cmd.workspaceControl(control_name, query=True, exists=True):
            cmd.workspaceControl(control_name, edit=True, close=True)
            cmd.deleteUI(control_name, control=True)

    # -------------------------------------------------------------------------------------------------------------- #

    def run(self):
        """
        Shows the window, docking it into the right place, inside Maya
        :return:
        """
        self.setObjectName('testWindow')
        workspace_control = self.objectName() + 'WorkspaceControl'

        self._deleteControl(workspace_control)


        self.show(dockable=True, area='right', floating=False)
        cmd.workspaceControl(workspace_control, edit=True, ttc=['AttributeEditor', -1], wp='preferred', mw=420)
        self.raise_()
        self.setDockableParameters(width=420)

# ------------------------------------------------------------------------------------------------------------------- #

def showMainWindow():
    """
    Shows the main windows and returns the window object created
    :return:
    """
    shaderGUI = SLUI(parent=getMayaMainWindow())
    shaderGUI.run()

    return shaderGUI




