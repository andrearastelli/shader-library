"""
SLTreeList.py

Shows a tree hierarchy of all the saved shaders.
The user can expand or contract each branch, and when one branch is selected, in the SLObjectList panel
a list of all the corresponding shaders will appear.
"""

try:
    from PySide2 import QtWidgets as gui
    from PySide2 import QtCore as core
except ImportError as ie:
    from PySide import QtGui as gui
    from PySide import QtCore as core


import maya.cmds as cmd



# ------------------------------------------------------------------------------------------------------------------- #

class SLTreeList(gui.QTreeWidget):


    def __init__(self):
        super(SLTreeList, self).__init__()

        self.setWindowTitle('Shader Hierarchy')

    # -------------------------------------------------------------------------------------------------------------- #

# ------------------------------------------------------------------------------------------------------------------- #