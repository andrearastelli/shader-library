"""
SLOBjectList.py

Panel that shows a grid of all the saved shaders that are linked to a specific branch in the tree list.
Each shader is rapresented by an image, and some information about it, like the name of the shader and other info
that the user can write when a new shader is saved.
"""

try:
    from PySide2 import QtWidgets as gui
    from PySide2 import QtCore as core
except ImportError as ie:
    from PySide import QtGui as gui
    from PySide import QtCore as core



# ------------------------------------------------------------------------------------------------------------------- #

class SLObjectList(gui.QWidget):

    listwidget = None

    def __init__(self):
        """

        """
        super(SLObjectList, self).__init__()

        self.setWindowTitle('Shader List')


        self.listwidget = gui.QListWidget()


        mainlayout = gui.QVBoxLayout()

        scrollarea = gui.QScrollArea()
        scrollarea.setWidget(self.listwidget)


        mainlayout.addWidget(scrollarea)

    # -------------------------------------------------------------------------------------------------------------- #

    def loadList(self, items=None):
        """

        :type items: list
        :param items: List if items that will fill the widget
        :return:
        """

        assert items, 'No items specified'
        assert isinstance(items, list), 'The items must be specified in a list'

        self.listwidget.clear()

        for item in items:

            listitem = gui.QListWidgetItem()
            listitem.setText(item)

            self.listwidget.addItem(listitem)

# ------------------------------------------------------------------------------------------------------------------- #
