from setuptools import setup
from setuptools import find_packages


with open('README.md') as f:
    readme = f.read()


with open('LICENSE') as f:
    lic = f.read()


setup(
    name='ShaderLibrary',
    version='0.0.1',
    description='A Shader Library for Maya',
    long_description=readme,
    author='Andrea Rastelli',
    author_email='rastelliandrea87@gmail.com',
    url='https://bitbucket.org/andrearastelli/shader-library',
    license=lic,
    packages=find_packages(exclude=('tests', 'docs'))
)
