# Shader Library #

This is a project inspired by Leonardo Dell'Agli, a Lighter that works with me in Rainbow CGI, in Rome.


He needs a tool to handles Shaders that is simple, reliable and that is fast enough to be used without any lag.


Also, his request was for a configurable environment, so each shader can have his own custom attributes and properties based on the rendering engine that the shader belongs to.